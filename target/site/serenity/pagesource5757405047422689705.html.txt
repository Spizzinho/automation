<!DOCTYPE html><!--[if lt IE 7 ]> <html lang="en" id="top" class="no-js ie6"> <![endif]--><!--[if IE 7 ]>    <html lang="en" id="top" class="no-js ie7"> <![endif]--><!--[if IE 8 ]>    <html lang="en" id="top" class="no-js ie8"> <![endif]--><!--[if IE 9 ]>    <html lang="en" id="top" class="no-js ie9"> <![endif]--><!--[if (gt IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" lang="en" id="top" class=" js no-touch localstorage" style=""><!--<![endif]--><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Men</title>
<meta name="description" content="Default Description" />
<meta name="keywords" content="Magento, Varien, E-commerce" />
<meta name="robots" content="INDEX,FOLLOW" />
<link rel="icon" href="http://cdn.magento-demo.lexiconn.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://cdn.magento-demo.lexiconn.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = 'http://cdn.magento-demo.lexiconn.com/js/blank.html';
    var BLANK_IMG = 'http://cdn.magento-demo.lexiconn.com/js/spacer.gif';
//]]>
</script>
<![endif]-->
<script type="text/javascript" src="http://cdn.magento-demo.lexiconn.com/media/js/c8ff984e301595e2901fcb196b5df92b.js"></script>
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Raleway:300,400,500,700,600" />
<link rel="canonical" href="http://magento-demo.lexiconn.com/men.html" />
<!--[if  (lte IE 8) & (!IEMobile)]>
<link rel="stylesheet" type="text/css" href="http://cdn.magento-demo.lexiconn.com/media/css/b6d1557cc17b17c1eacb41dbdb6ea7c5.css" media="all" />
<![endif]-->
<!--[if (gte IE 9) | (IEMobile)]><!-->
<link rel="stylesheet" type="text/css" href="http://cdn.magento-demo.lexiconn.com/media/css/d02ceb68f03f57aefc579ea0c1411fc1.css" media="all" />
<!--<![endif]-->

<script type="text/javascript">
//&lt;![CDATA[
Mage.Cookies.path     = '/';
Mage.Cookies.domain   = '.magento-demo.lexiconn.com';
//]]&gt;
</script>
<meta name="viewport" content="initial-scale=1.0, width=device-width" />

<script type="text/javascript">
//&lt;![CDATA[
optionalZipCountries = ["HK","IE","MO","PA"];
//]]&gt;
</script>
<script type="text/javascript">//&lt;![CDATA[
        var Translator = new Translate([]);
        //]]&gt;</script></head>
<body class=" catalog-category-view categorypath-men-html category-men" style="">
<div class="wrapper">
        <noscript>
        &lt;div class="global-site-notice noscript"&gt;
            &lt;div class="notice-inner"&gt;
                &lt;p&gt;
                    &lt;strong&gt;JavaScript seems to be disabled in your browser.&lt;/strong&gt;&lt;br /&gt;
                    You must have JavaScript enabled in your browser to utilize the functionality of this website.                &lt;/p&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    </noscript>
    <div class="global-site-notice demo-notice">
        <div class="notice-inner"><p>This is a demo Magento 1.9 store, hosted on a standard VPS at <b><a href="http://www.lexiconn.com/ecommerce/magento/">LexiConn</a></b>. This is the speed and performance you can expect by hosting your Magento store with us.</p></div>
    </div>
    <div class="page">
        
<div class="header-language-background">
    <div class="header-language-container">
        <div class="store-language-container">
            <div class="form-language">
    <label for="select-language">Your Language:</label>
    <select id="select-language" title="Your Language" onchange="window.location.href=this.value">
                    <option value="http://magento-demo.lexiconn.com/men.html?___store=default&amp;___from_store=default" selected="selected">English</option>
                    <option value="http://magento-demo.lexiconn.com/men.html?___store=french&amp;___from_store=default">French</option>
                    <option value="http://magento-demo.lexiconn.com/men.html?___store=german&amp;___from_store=default">German</option>
        </select>
</div>
        </div>

        
        <p class="welcome-msg"><!--ewpagecache:welcome_message_block_begin:-->Welcome<!--ewpagecache:welcome_message_block_end--> </p>
    </div>
</div>

<header id="header" class="page-header">
    <div class="page-header-container">
        <a class="logo" href="http://magento-demo.lexiconn.com/">
            <img src="http://cdn.magento-demo.lexiconn.com/skin/frontend/rwd/default/images/media/logo.png" alt="Madison Island" class="large" />
            <img src="http://cdn.magento-demo.lexiconn.com/skin/frontend/rwd/default/images/media/logo_small.png" alt="Madison Island" class="small" />
        </a>

                <div class="store-language-container"></div>

        <!-- Skip Links -->

        <div class="skip-links">
            <a href="#header-nav" class="skip-link skip-nav">
                <span class="icon"></span>
                <span class="label">Menu</span>
            </a>

            <a href="#header-search" class="skip-link skip-search">
                <span class="icon"></span>
                <span class="label">Search</span>
            </a>

            <a href="#header-account" class="skip-link skip-account">
                <span class="icon"></span>
                <span class="label">Account</span>
            </a>

            <!-- Cart -->

            <div class="header-minicart">
                

<a href="#header-cart" class="skip-link skip-cart  no-count">
    <span class="icon"></span>
    <span class="label">Cart</span>
    <span class="count">0</span>
</a>

<div id="header-cart" class="block block-cart skip-content">
    <!--ewpagecache:checkout_cart_sidebar_begin:7949f-->
<div id="minicart-error-message" class="minicart-message"></div>
<div id="minicart-success-message" class="minicart-message"></div>

<div class="minicart-wrapper">

    <p class="block-subtitle">
        Recently added item(s)        <a class="close skip-link-close" href="#" title="Close">×</a>
    </p>

                    <p class="empty">You have no items in your shopping cart.</p>

    </div>
<!--ewpagecache:checkout_cart_sidebar_end--></div>
            </div>


        </div>

        <!-- Navigation -->

        <div id="header-nav" class="skip-content">
            
    <nav id="nav">
        <ol class="nav-primary">
            <li class="level0 nav-1 first parent"><a href="http://magento-demo.lexiconn.com/women.html" class="level0 has-children">Women</a><ul class="level0"><li class="level1"><a class="level1" href="http://magento-demo.lexiconn.com/women.html">View All Women</a></li><li class="level1 nav-1-1 first"><a href="http://magento-demo.lexiconn.com/women/new-arrivals.html" class="level1 ">New Arrivals</a></li><li class="level1 nav-1-2"><a href="http://magento-demo.lexiconn.com/women/tops-blouses.html" class="level1 ">Tops &amp; Blouses</a></li><li class="level1 nav-1-3"><a href="http://magento-demo.lexiconn.com/women/pants-denim.html" class="level1 ">Pants &amp; Denim</a></li><li class="level1 nav-1-4 last"><a href="http://magento-demo.lexiconn.com/women/dresses-skirts.html" class="level1 ">Dresses &amp; Skirts</a></li></ul></li><li class="level0 nav-2 active parent"><a href="http://magento-demo.lexiconn.com/men.html" class="level0 has-children">Men</a><ul class="level0"><li class="level1"><a class="level1" href="http://magento-demo.lexiconn.com/men.html">View All Men</a></li><li class="level1 nav-2-1 first"><a href="http://magento-demo.lexiconn.com/men/new-arrivals.html" class="level1 ">New Arrivals</a></li><li class="level1 nav-2-2"><a href="http://magento-demo.lexiconn.com/men/shirts.html" class="level1 ">Shirts</a></li><li class="level1 nav-2-3"><a href="http://magento-demo.lexiconn.com/men/tees-knits-and-polos.html" class="level1 ">Tees, Knits and Polos</a></li><li class="level1 nav-2-4"><a href="http://magento-demo.lexiconn.com/men/pants-denim.html" class="level1 ">Pants &amp; Denim</a></li><li class="level1 nav-2-5 last"><a href="http://magento-demo.lexiconn.com/men/blazers.html" class="level1 ">Blazers</a></li></ul></li><li class="level0 nav-3 parent"><a href="http://magento-demo.lexiconn.com/accessories.html" class="level0 has-children">Accessories</a><ul class="level0"><li class="level1"><a class="level1" href="http://magento-demo.lexiconn.com/accessories.html">View All Accessories</a></li><li class="level1 nav-3-1 first"><a href="http://magento-demo.lexiconn.com/accessories/eyewear.html" class="level1 ">Eyewear</a></li><li class="level1 nav-3-2"><a href="http://magento-demo.lexiconn.com/accessories/jewelry.html" class="level1 ">Jewelry</a></li><li class="level1 nav-3-3"><a href="http://magento-demo.lexiconn.com/accessories/shoes.html" class="level1 ">Shoes</a></li><li class="level1 nav-3-4 last"><a href="http://magento-demo.lexiconn.com/accessories/bags-luggage.html" class="level1 ">Bags &amp; Luggage</a></li></ul></li><li class="level0 nav-4 parent"><a href="http://magento-demo.lexiconn.com/home-decor.html" class="level0 has-children">Home &amp; Decor</a><ul class="level0"><li class="level1"><a class="level1" href="http://magento-demo.lexiconn.com/home-decor.html">View All Home &amp; Decor</a></li><li class="level1 nav-4-1 first"><a href="http://magento-demo.lexiconn.com/home-decor/books-music.html" class="level1 ">Books &amp; Music</a></li><li class="level1 nav-4-2"><a href="http://magento-demo.lexiconn.com/home-decor/bed-bath.html" class="level1 ">Bed &amp; Bath</a></li><li class="level1 nav-4-3"><a href="http://magento-demo.lexiconn.com/home-decor/electronics.html" class="level1 ">Electronics</a></li><li class="level1 nav-4-4 last"><a href="http://magento-demo.lexiconn.com/home-decor/decorative-accents.html" class="level1 ">Decorative Accents</a></li></ul></li><li class="level0 nav-5 parent"><a href="http://magento-demo.lexiconn.com/sale.html" class="level0 has-children">Sale</a><ul class="level0"><li class="level1"><a class="level1" href="http://magento-demo.lexiconn.com/sale.html">View All Sale</a></li><li class="level1 nav-5-1 first"><a href="http://magento-demo.lexiconn.com/sale/women.html" class="level1 ">Women</a></li><li class="level1 nav-5-2"><a href="http://magento-demo.lexiconn.com/sale/men.html" class="level1 ">Men</a></li><li class="level1 nav-5-3"><a href="http://magento-demo.lexiconn.com/sale/accessories.html" class="level1 ">Accessories</a></li><li class="level1 nav-5-4 last"><a href="http://magento-demo.lexiconn.com/sale/home-decor.html" class="level1 ">Home &amp; Decor</a></li></ul></li><li class="level0 nav-6 last"><a href="http://magento-demo.lexiconn.com/vip.html" class="level0 ">VIP</a></li>        </ol>
    </nav>
        </div>

        <!-- Search -->

        <div id="header-search" class="skip-content">
            
<form id="search_mini_form" action="http://magento-demo.lexiconn.com/catalogsearch/result/" method="get">
    <div class="input-box">
        <label for="search">Search:</label>
        <input id="search" type="search" name="q" value="" class="input-text required-entry" maxlength="128" placeholder="Search entire store here..." autocomplete="off" />
        <button type="submit" title="Search" class="button search-button"><span><span>Search</span></span></button>
    </div>

    <div id="search_autocomplete" class="search-autocomplete" style="display: none;"></div>
    <script type="text/javascript">
    //&lt;![CDATA[
        var searchForm = new Varien.searchForm('search_mini_form', 'search', '');
        searchForm.initAutocomplete('http://magento-demo.lexiconn.com/catalogsearch/ajax/suggest/', 'search_autocomplete');
    //]]&gt;
    </script>
</form>
        </div>

        <!-- Account -->

        <div id="header-account" class="skip-content">
            <div class="links">
        <ul>
                                    <li class="first"><a href="http://magento-demo.lexiconn.com/customer/account/" title="My Account">My Account</a></li>
                                                <!--ewpagecache:wishlist_links_begin:88ba3--><li><a href="http://magento-demo.lexiconn.com/wishlist/" title="My Wishlist">My Wishlist</a></li>
<!--ewpagecache:wishlist_links_end-->                                                <li><!--ewpagecache:toplink_cart_begin:--><a href="http://magento-demo.lexiconn.com/checkout/cart/" title="My Cart" class="top-link-cart">My Cart</a><!--ewpagecache:toplink_cart_end--></li>
                                                <li><a href="http://magento-demo.lexiconn.com/checkout/" title="Checkout" class="top-link-checkout">Checkout</a></li>
                                                <li><a href="http://magento-demo.lexiconn.com/customer/account/create/" title="Register">Register</a></li>
                                                <li class=" last"><!--ewpagecache:toplink_login_begin:--><a href="http://magento-demo.lexiconn.com/customer/account/login/" title="Log In">Log In</a><!--ewpagecache:toplink_login_end--></li>
                        </ul>
</div>
        </div>

        <!-- Cart -->

        <!-- <div id="header-cart" class="skip-content">
            <div class="mini-cart">
                <div class="menu-wrapper">
                    <div class="menu cart-menu">
                        <div class="empty">Your cart is empty.</div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</header>


        <div class="main-container col1-layout">
            <div class="main">
                <div class="breadcrumbs">
    <ul>
                    <li class="home">
                            <a href="http://magento-demo.lexiconn.com/" title="Go to Home Page">Home</a>
                                        <span>/ </span>
                        </li>
                    <li class="category5">
                            <strong>Men</strong>
                                    </li>
            </ul>
</div>
                <div class="col-main">
                    <!--ewpagecache:core_messages_begin:08220--><!--ewpagecache:core_messages_end-->                    <div class="page-title category-title">
        <h1>Men</h1>
</div>

<!--ewpagecache:core_messages_begin:f97cc--><!--ewpagecache:core_messages_end-->


    <div class="category-image">
    <img src="http://cdn.magento-demo.lexiconn.com/media/wysiwyg/catmen-newimg.jpg" alt="Bold New Blues - superior fabrics and tailored fits in subtle and standout hues" />
</div>

<ul class="catblocks">
    <li>
        <a href="http://magento-demo.lexiconn.com/men/new-arrivals.html">
            <img src="http://cdn.magento-demo.lexiconn.com/media/wysiwyg/mencat-newarrivalsimg.jpg" alt="New Arrivals" />
            <span>New Arrivals</span>
        </a>
    </li>
    <li>
        <a href="http://magento-demo.lexiconn.com/men/shirts.html">
            <img src="http://cdn.magento-demo.lexiconn.com/media/wysiwyg/clp-sub-m-shirts.jpg" alt="Shirts" />
            <span>Shirts</span>
        </a>
    </li>
    <li>
        <a href="http://magento-demo.lexiconn.com/men/tees-knits-and-polos.html">
            <img src="http://cdn.magento-demo.lexiconn.com/media/wysiwyg/Mencat-teesimg.jpg" alt="Tees, Knits and Polos" />
            <span>Tees, Knits and Polos</span>
        </a>
    </li>
    <li>
        <a href="http://magento-demo.lexiconn.com/men/pants-denim.html">
            <img src="http://cdn.magento-demo.lexiconn.com/media/wysiwyg/mencat-pants.jpg" alt="Pants &amp; Denim" />
            <span>Pants &amp; Denim</span>
        </a>
    </li>
</ul>
<div id="map-popup" class="map-popup" style="display:none;">
    <a href="#" class="map-popup-close" id="map-popup-close">×</a>
    <div class="map-popup-heading"><h3 id="map-popup-heading"></h3></div>
    <div class="map-popup-content" id="map-popup-content">
        <div class="map-popup-msrp" id="map-popup-msrp-box">
            <span class="label">Price:</span>
            <span style="text-decoration:line-through;" id="map-popup-msrp"></span>
        </div>
        <div class="map-popup-price" id="map-popup-price-box">
            <span class="label">Actual Price:</span>
            <span id="map-popup-price"></span>
        </div>
        <div class="map-popup-checkout">
            <form action="" method="POST" id="product_addtocart_form_from_popup">
                <input type="hidden" name="product" class="product_id" value="" id="map-popup-product-id" />
                <div class="additional-addtocart-box">
                                    </div>
                <button type="button" title="Add to Cart" class="button btn-cart" id="map-popup-button"><span><span>Add to Cart</span></span></button>
            </form>
        </div>
        <script type="text/javascript">
        //&lt;![CDATA[
            document.observe("dom:loaded", Catalog.Map.bindProductForm);
        //]]&gt;
        </script>
    </div>
    <div class="map-popup-text" id="map-popup-text">Our price is lower than the manufacturer's "minimum advertised price."  As a result, we cannot show you the price in catalog or the product page. <br /><br /> You have no obligation to purchase the product once you know the price. You can simply remove the item from your cart.</div>
    <div class="map-popup-text" id="map-popup-text-what-this">Our price is lower than the manufacturer's "minimum advertised price."  As a result, we cannot show you the price in catalog or the product page. <br /><br /> You have no obligation to purchase the product once you know the price. You can simply remove the item from your cart.</div>
</div>
                </div>
            </div>
        </div>
                <div class="footer-container">
    <div class="footer">
        <div class="block block-subscribe">
    <div class="block-title">
        <strong><span>Newsletter</span></strong>
    </div>
    <form action="http://magento-demo.lexiconn.com/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
        <div class="block-content">
            <div class="form-subscribe-header">
                <label for="newsletter">Sign Up for Our Newsletter:</label>
            </div>
            <div class="input-box">
               <input type="email" autocapitalize="off" autocorrect="off" spellcheck="false" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email" />
            </div>
            <div class="actions">
                <button type="submit" title="Subscribe" class="button"><span><span>Subscribe</span></span></button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
    //&lt;![CDATA[
        var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
    //]]&gt;
    </script>
</div>
<div class="links">
	<div class="block-title">
		<strong><span>Company</span></strong>
	</div>
	<ul>
		<li><a href="http://magento-demo.lexiconn.com/about-magento-demo-store/">About Us</a></li>
		<li><a href="http://magento-demo.lexiconn.com/contacts/">Contact Us</a></li>
		<li><a href="http://magento-demo.lexiconn.com/customer-service/">Customer Service</a></li>
		<li><a href="http://magento-demo.lexiconn.com/privacy-policy-cookie-restriction-mode/">Privacy Policy</a></li>
	</ul>
</div><div class="links">
        <div class="block-title"><strong><span>Quick Links</span></strong></div>
        <ul>
                                    <li class="first"><a href="http://magento-demo.lexiconn.com/catalog/seo_sitemap/category/" title="Site Map">Site Map</a></li>
                                                <li><a href="http://magento-demo.lexiconn.com/catalogsearch/term/popular/" title="Search Terms">Search Terms</a></li>
                                                <li class=" last"><a href="http://magento-demo.lexiconn.com/catalogsearch/advanced/" title="Advanced Search">Advanced Search</a></li>
                        </ul>
</div>
<div class="links">
        <div class="block-title"><strong><span>Account</span></strong></div>
        <ul>
                                    <li class="first"><a href="http://magento-demo.lexiconn.com/customer/account/" title="My Account">My Account</a></li>
                                                <li class=" last"><a href="http://magento-demo.lexiconn.com/sales/guest/form/" title="Orders and Returns">Orders and Returns</a></li>
                        </ul>
</div>
<div class="links social-media">
	<div class="block-title">
		<strong><span>Connect With Us</span></strong>
	</div>
	<ul>
		<li><a href="#"><em class="facebook"></em>Facebook</a></li>
		<li><a href="#"><em class="twitter"></em>Twitter</a></li>
		<li><a href="#"><em class="youtube"></em>YouTube</a></li>
		<li><a href="#"><em class="pinterest"></em>Pinterest</a></li>
		<li class="last"><a href="#"><em class="rss"></em>RSS</a></li>
	</ul>
</div>        <address class="copyright">© 2014 Madison Island. All Rights Reserved.</address>
    </div>
</div>
                

    </div>
</div>
<!--c1f273acf6a6183e9348c610b8dbda6f-->
</body></html>