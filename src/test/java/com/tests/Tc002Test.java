package com.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.steps.serenity.HeaderSteps;
import com.steps.serenity.MainCategorySteps;
import com.steps.serenity.SubcategorySteps;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class Tc002Test {
	@Managed(uniqueSession = true)
    public WebDriver webdriver;
	
	@Steps
	public HeaderSteps headerSteps;
	
	@Steps
	public MainCategorySteps mainCategorySteps;
	
	@Steps
	public SubcategorySteps subcategorySteps;
	
	String url = "http://magento-demo.lexiconn.com/";
	
	@Test
	public void Tc002Test(){
		headerSteps.openHomepageAndSelectNavElement(url);
		mainCategorySteps.selectSubcategory();
//		mainCategorySteps.clickOnSubcategoryOne();
		subcategorySteps.selectProduct();
		
//		subcategorySteps.clickOnViewDetailsButton();
	}

}
