package com.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.steps.serenity.HeaderSteps;
import com.steps.serenity.MainCategorySteps;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class SelectProductTest {
	@Managed(uniqueSession = true)
    public WebDriver webdriver;
	
	@Steps
	public HeaderSteps headerSteps;
	
	@Steps
	public MainCategorySteps mainCategorySteps;
	
	String url = "http://magento-demo.lexiconn.com/";
	String navCat1 = "MEN";
	String navCat2 = "WOMEN";
	String navCat3 = "Sale";
	String navCat4 = "Accessories";
	String navDropdown = "Tees, Knits and Polos";
			
	@Test
	public void selectProductTest(){
		headerSteps.openHomepage(url);
		headerSteps.selectNavCategory(navCat1);
		headerSteps.selectNavDropdownElement(navDropdown);
//		mainCategorySteps.clickOnSubcategoryOne();
//		headerSteps.selectNavCategory(navCat2);
//		mainCategorySteps.clickOnSubcategoryOne();
//		headerSteps.selectNavCategory(navCat3);
//		headerSteps.selectNavCategory(navCat4);
		
	}
}
 