package com.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.steps.serenity.CheckoutSteps;
import com.steps.serenity.HeaderSteps;
import com.steps.serenity.MainCategorySteps;
import com.steps.serenity.SubcategorySteps;
import com.steps.serenity.ProductDetailsSteps;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class Tc001Test {
	@Managed(uniqueSession = true)
    public WebDriver webdriver;
	
	@Steps
	public HeaderSteps headerSteps;
	
	@Steps
	public MainCategorySteps mainCategorySteps;
	
	@Steps
	public SubcategorySteps subcategorySteps;
	
	@Steps
	public ProductDetailsSteps productDetailsSteps;
	
	@Steps
	public CheckoutSteps checkoutSteps;
	
	String url = "http://magento-demo.lexiconn.com/";
	String navCat1 = "MEN";
	String navCat2 = "WOMEN";
	String navCat3 = "SALE";
	String navCat4 = "ACCESSORIES";
	String qty = "2";
	String subcat1 = "TEES, KNITS AND POLOS";
	
			
	@Test
	public void selectProductTest(){
		headerSteps.openHomepage(url);
//		headerSteps.openHomepageAndSelectNavElement(url);

		headerSteps.clickNavMenElement();
		//TODO : verifica daca sunt produse
			
		mainCategorySteps.clickOnSubcategoryOne();
		
		//TODO : daca nu exista produse, intra in alta subcategorie
		subcategorySteps.clickOnBreadCrumbsToCategory();
		
		//TODO : verifica daca sunt produse
		mainCategorySteps.clickOnSubcategoryFour();
		
		//TODO : pick random product from the ones in the category
		subcategorySteps.clickOnViewDetailsButton();
		productDetailsSteps.clickOnColorDropdown();
		
		//TODO : pick random color from the available colors in the list
		productDetailsSteps.clickOnColorOption();
		productDetailsSteps.clickOnSizeDropdown();
		
		//TODO : pick random size from the available sizes in the list
		productDetailsSteps.clickOnSizeOption();
		
		//TODO : pick random number (ranges from 1 to 4)
		productDetailsSteps.inputQty();
		productDetailsSteps.clickOnAddToCartButton();
		
		// new one
		checkoutSteps.cartCheckout();
		
		
		
		
//		
//		headerSteps.clickNavWomenElement();
//		mainCategorySteps.clickOnSubcategoryOne();
//		headerSteps.clickNavVipElement();
//		headerSteps.selectNavCategory(navCat1);
//		headerSteps.selectNavDropdownElement(subcat1);
		//TODO : click or hover and click on dropdown elements
		
//		headerSteps.selectNavCategory(navCat4);
		
		//TODO : click or hover and click on dropdown elements

		
		// TODO : checkout
		
	}
}
