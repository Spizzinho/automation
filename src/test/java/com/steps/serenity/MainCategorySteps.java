package com.steps.serenity;

import com.pages.MainCategoryPage;
import com.utils.RandomGenerators;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

@SuppressWarnings("serial")
public class MainCategorySteps extends ScenarioSteps{
	MainCategoryPage mainCategoryPage;
	RandomGenerators randomGenerators;
	
	@Step
	public void selectSubcategory(){
//	1	generate random nr
		System.out.println("merge pana la randomul de pe main category");
		switch(randomGenerators.getRandomIntegerBetween(0,3)) {
		   case 0 : clickOnSubcategoryOne(); break; 
		   case 1 : clickOnSubcategoryTwo(); break;		      
		   case 2 : clickOnSubcategoryThree(); break;		      
		   case 3 : clickOnSubcategoryFour(); break;		   
		   default : System.out.println("nu merge randomul de pe main category");
		}
//	3	verifica daca sunt produse si alege unu
	}
	
	@Step
	public void clickOnSubcategoryOne(){
		mainCategoryPage.clickOnSubcategoryOne();
	}
	
	@Step
	public void clickOnSubcategoryTwo(){
		mainCategoryPage.clickOnSubcategoryTwo();
	}
	
	@Step
	public void clickOnSubcategoryThree(){
		mainCategoryPage.clickOnSubcategoryThree();
	}
	
	@Step
	public void clickOnSubcategoryFour(){
		mainCategoryPage.clickOnSubcategoryFour();
	}

}
