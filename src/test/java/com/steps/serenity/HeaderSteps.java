package com.steps.serenity;

import java.util.ArrayList;
import java.util.List;

import com.pages.HeaderPage;
import com.utils.StringUtils;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

@SuppressWarnings("serial")
public class HeaderSteps extends ScenarioSteps {
	HeaderPage headerPage;
	StringUtils stringUtils;
	
	@Step
	public void openHomepageAndSelectNavElement(String url){
		openHomepage(url);
		randomListItem();
	}
	
	@Step
	public <T> void randomListItem() {
		List<T> listOfElements = new ArrayList<T>();
		listOfElements.add((T) "WOMEN");
		listOfElements.add((T) "MEN");
		listOfElements.add((T) "ACCESSORIES");
		listOfElements.add((T) "HOME & DECOR");
//		listOfElements.add((T) "SALE");
		String element = (String)(stringUtils.getRandomItemFromList(listOfElements));
		clickNavCategory(element);
	}
	
	@Step
	public void openHomepage(String url) {
		headerPage.getDriver().manage().window().maximize();
		headerPage.getDriver().get(url);
	}
		
	@Step
	public void selectNavCategory(String text){
		headerPage.selectNavElement(text);
	}
	
	@Step
	public void clickNavCategory(String text){
		headerPage.clickNavElement(text);
	}
	
	@Step
	public void selectNavDropdownElement(String text){
		headerPage.selectNavDropdownElement(text);
	}
	
	@Step
	public void clickNavWomenElement(){
		headerPage.clickNavWomenElement();
	}
	
	@Step
	public void clickNavMenElement(){
		headerPage.clickNavMenElement();
	}
	
	@Step
	public void clickNavAccessoriesElement(){
		headerPage.clickNavAccessoriesElement();
	}
	
	@Step
	public void clickNavHomedecorElement(){
		headerPage.clickNavHomedecorElement();
	}
	
	@Step
	public void clickNavSaleElement(){
		headerPage.clickNavSaleElement();
	}
	
	@Step
	public void clickNavVipElement(){
		headerPage.clickNavVipElement();
	}
	

}
