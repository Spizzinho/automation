package com.steps.serenity;

import com.pages.CheckoutPage;
import com.pages.ShoppingCartPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

@SuppressWarnings("serial")
public class CheckoutSteps extends ScenarioSteps{
	CheckoutPage checkoutPage;
	ShoppingCartPage shoppingCartPage;
	
	
	
	@Step
	public void cartCheckout(){
		String first = "Luca";
		String last = "Brano";
		String company = "Bad";
		String email = "luca.brano@bad.com";
		String address = "Motilor, 62A";
		String city = "Cluj-Napoca";
		String zip = "400522";
		String telephone = "0799000111";
				
		clickShoppingCartCheckoutButton();
		clickOnChechoutAsGuest();
		clickOnContinueButton();
		enterFirstname(first);
		enterLastname(last);
		enterCompany(company);
		enterEmail(email);
		enterAddress(address);
		enterCity(city);
		clickOnCountryDropdown();
		clickOnCountryDropdownOption();
		clickOnRegionDropdown();
		clickOnRegionDropdownOption();
		enterZipcode(zip);
		enterTelephone(telephone);
		clickOnShipToAddressRadio();
		clickOnBillingInfoContinueButton();
		clickOnFirstShippingMethodRadio();
		clickOnShippingContinueButton();
		clickOnPaymentContinueButton();
		clickOnPlaceOrderButton();
		clickOnContinueShoppingButton();
	}
	
	@Step
	public void clickShoppingCartCheckoutButton(){
		shoppingCartPage.clickOnCheckoutButton();
	}
	
	@Step
	public void clickOnChechoutAsGuest(){
		checkoutPage.clickOnChechoutAsGuest();
	}
	
	@Step
	public void clickOnContinueButton(){
		checkoutPage.clickOnContinueButton();
	}
	
	@Step
	public void enterFirstname(String text){
		checkoutPage.enterFirstname(text);
	}
	
	@Step
	public void enterLastname(String text){
		checkoutPage.enterLastname(text);
	}
	
	@Step
	public void enterCompany(String text){
		checkoutPage.enterCompany(text);
	}
	
	@Step
	public void enterEmail(String text){
		checkoutPage.enterEmail(text);
	}
	
	@Step
	public void enterAddress(String text){
		checkoutPage.enterAddress(text);
	}
	
	@Step
	public void enterCity(String text){
		checkoutPage.enterCity(text);
	}
	
	@Step
	public void clickOnCountryDropdown(){
		checkoutPage.clickOnCountryDropdown();
	}
	
	@Step
	public void clickOnCountryDropdownOption(){
		checkoutPage.clickOnCountryDropdownOption();
	}
	
	@Step
	public void clickOnRegionDropdown(){
		checkoutPage.clickOnRegionDropdownn();
	}
	
	@Step
	public void clickOnRegionDropdownOption(){
		checkoutPage.clickOnRegionDropdownnOption();
	}
	
	@Step
	public void enterZipcode(String text){
		checkoutPage.enterZipcode(text);
	}
	
	@Step
	public void enterTelephone(String text){
		checkoutPage.enterTelephone(text);
	}

	@Step
	public void clickOnShipToAddressRadio(){
		checkoutPage.clickOnShipToAddressRadio();
	}
	
	@Step
	public void clickOnBillingInfoContinueButton(){
		checkoutPage.clickOnBillingInfoContinueButton();
	}
	
	@Step
	public void clickOnFirstShippingMethodRadio(){
		checkoutPage.clickOnFirstShippingMethodRadio();
	}
	
	@Step
	public void clickOnShippingContinueButton(){
		checkoutPage.clickOnShippingContinueButton();
	}
	
	@Step
	public void clickOnPaymentContinueButton(){
		checkoutPage.clickOnPaymentContinueButton();
	}
	
	@Step
	public void clickOnPlaceOrderButton(){
		checkoutPage.clickOnPlaceOrderButton();
	}		
	
	@Step
	public void clickOnContinueShoppingButton(){
		checkoutPage.clickOnContinueShoppingButton();
	}
	
}
