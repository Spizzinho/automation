package com.steps.serenity;

import com.pages.ProductDetailsPage;
import com.utils.RandomGenerators;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

@SuppressWarnings("serial")
public class ProductDetailsSteps extends ScenarioSteps{
	ProductDetailsPage productDetailsPage;
	RandomGenerators randomGenerators;
	
	@Step
	public void addProductToCart(){
		if(checkForDropdown()){
			inputQty();
			clickOnAddToCartButton();
		}else{
			clickOnColorDropdown();
			clickOnColorOption();
			clickOnSizeDropdown();
			clickOnSizeOption();
			inputQty();
			clickOnAddToCartButton();
		}
	}
	
	@Step
	public boolean checkForDropdown(){
		return productDetailsPage.checkForDropdown();
	}
	
	@Step
	public void clickOnColorDropdown(){
		productDetailsPage.clickOnColorDropdown();
	}
	
	@Step
	public void clickOnColorOption(){
		productDetailsPage.clickOnColorOption();
	}
	
	@Step
	public void clickOnSizeDropdown(){
		productDetailsPage.clickOnSizeDropdown();
	}
	
	@Step
	public void clickOnSizeOption(){
		productDetailsPage.clickOnSizeOption();
	}
	
	@Step
	public void inputQty(){
		int x = 3;
		productDetailsPage.inputQty(RandomGenerators.getRandomInteger(x));
	}
		
	@Step
	public void clickOnAddToCartButton(){
		productDetailsPage.clickOnAddToCartButton();
	}
	
	@Step
	public void clickOnAddToWishlistLink(){
		productDetailsPage.clickOnAddToWishlistLink();
	}
	
	@Step
	public void clickOnAddToCompareLink(){
		productDetailsPage.clickOnAddToCompareLink();
	}
}
