package com.steps.serenity;

import com.pages.SubcategoryPage;
import com.utils.RandomGenerators;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

@SuppressWarnings("serial")
public class SubcategorySteps extends ScenarioSteps{
	SubcategoryPage subcategoryPage;
	RandomGenerators randomGenerators;
	MainCategorySteps mainCategorySteps;
	
	@Step
	public void selectProduct(){
		System.out.println("merge pana la selectProduct");
//		int count = 1;
			if(subcategoryPage.checkForProducts()>0){
				System.out.println("if true selectProduct");
				clickOnViewDetailsButton();
			}else{
				System.out.println("if false selectProduct");
				clickOnBreadCrumbsToCategory();
//				mainCategorySteps.selectSubcategory();
//				selectProduct();
			}
	}
		
	@Step
	public void clickOnBreadCrumbsToCategory(){
		subcategoryPage.clickOnBreadCrumbsToCategory();
	}

	@Step
	public void clickOnViewDetailsButton(){
		subcategoryPage.clickOnViewDetailsButton();
	}
	
	@Step
	public void clickOnAddToWishlistLink(){
		subcategoryPage.clickOnAddToWishlistLink();
	}
	
	@Step
	public void clickOnAddToCompareLink(){
		subcategoryPage.clickOnAddToCompareLink();
	}
	
}
