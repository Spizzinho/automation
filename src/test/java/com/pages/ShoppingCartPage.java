package com.pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class ShoppingCartPage extends PageObject{
	
	@FindBy(css = "#country")
	public WebElement countryDropdown;
	
	@FindBy(css = "#country > option:nth-child(181)")
	public WebElement countryOptionsContainer;
	
	@FindBy(css = "#region_id")
	public WebElement regionDropdown;
	
	@FindBy(css = "#region_id > option:nth-child(15)")
	public WebElement regionOptionsContainer;
	
	@FindBy(css = "#city")
	public WebElement cityInput;
	
	@FindBy(css = "#postcode")
	public WebElement postcodeInput;
	
	@FindBy(css = "#shipping-zip-form > div > button > span > span")
	public WebElement estimateButton;
	
	@FindBy(css = "#co-shipping-method-form > dl > dd > ul > li > label")
	public WebElement deliveryOptionsContainer;
	
	@FindBy(css = "#co-shipping-method-form > div > button")
	public WebElement updateTotalButton;
	
	@FindBy(css = "#shopping-cart-totals-table > tfoot > tr > td:nth-child(2) > strong > span")
	public WebElement grandTotalSpan;
	
	@FindBy(css = ".method-checkout-cart-methods-onepage-bottom > button.btn-checkout > span > span")
	public WebElement checkoutButton;
	

	
	public void clickOnCountryDropdown(){
		countryDropdown.click();
	}
	
	public void clickOnCountryDropdownOption(){
		countryOptionsContainer.click();
	}

	public void clickOnRegionDropdown(){
		regionDropdown.click();
	}
	
	public void clickOnRegionDropdownOption(){
		regionOptionsContainer.click();
	}
	
	//TODO CITY, POSTCODE SI ESTIMATE
	
	public void clickOnCheckoutButton(){
		checkoutButton.click();
	}
}
