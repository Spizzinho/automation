package com.pages;

import java.util.NoSuchElementException;
import java.util.Random;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class ProductDetailsPage extends PageObject{
	Random rand;
	
	@FindBy (css="#attribute92")
	public WebElement colorDropdown;
	
	//todo : urmeaza sa fac lista
	@FindBy (css="#attribute92 > option:nth-child(2)")
	public WebElement colorOption;
	
	@FindBy (css="#attribute180")
	public WebElement sizeDropdown;
	
	//todo : urmeaza sa fac lista
	@FindBy (css="#attribute180 > option:nth-child(2)")
	public WebElement sizeOption;
	
	@FindBy (css="#qty")
	public WebElement qtyInput;
	
	@FindBy (css=".add-to-cart-buttons > button")
	public WebElement addToCartButton;

	@FindBy (css=".add-to-links > li:nth-child(1) > a")
	public WebElement addToWishlistLink;
	
	@FindBy (css=".add-to-links > li:nth-child(2) > a")
	public WebElement addToCompareLink;
	


	public boolean checkForDropdown(){
		boolean present;
		if (getDriver().findElements(By.id("attribute92")).isEmpty()){;
			present = true;
		}else{
			present = false;}
		return present;
	}
	
	public void clickOnColorDropdown(){
		colorDropdown.click();
	}
	
	public void clickOnColorOption(){
		colorOption.click();
	}
	
	public void clickOnSizeDropdown(){
		sizeDropdown.click();
	}
	
	public void clickOnSizeOption(){
		sizeOption.click();
	}
	
	public void inputQty(Integer integer){
		int x = integer + 1;
		String qty = String.valueOf(x);
		qtyInput.clear();
		qtyInput.sendKeys(qty);	
	}
		
	public void clickOnAddToCartButton(){
		addToCartButton.click();
	}
	
	public void clickOnAddToWishlistLink(){
		addToWishlistLink.click();
	}
	
	public void clickOnAddToCompareLink(){
		addToCompareLink.click();
	}
}
