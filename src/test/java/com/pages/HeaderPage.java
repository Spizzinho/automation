package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class HeaderPage extends PageObject{
	
//	TODO create navbar elements list
	@FindBy(css = ".nav-primary>li")
	public WebElement navElementContainer;
	
	@FindBy(css = ".nav-primary>li.active > ul > li")
	public WebElement navElementDropdownContainer;
	
	@FindBy(css = ".nav-primary>li:nth-child(1)")
	public WebElement navWomenElement;
	
	@FindBy(css = ".nav-primary>li:nth-child(2)")
	public WebElement navMenElement;
	
	@FindBy(css = ".nav-primary>li:nth-child(3)")
	public WebElement navAccessoriesElement;
	
	@FindBy(css = ".nav-primary>li:nth-child(4)")
	public WebElement navHomedecorElement;
	
	@FindBy(css = ".nav-primary>li:nth-child(5)")
	public WebElement navSaleElement;
	
	@FindBy(css = ".nav-primary>li:nth-child(6)>a")
	public WebElement navVipElement;
	
	
//	TODO create navbar elements list
	public void selectNavElement(String selectNavElement) {
		List<WebElement> navElementList = navElementContainer.findElements(
				By.xpath("//*[@id='nav']/ol/li/a"));
		Actions actions = new Actions(getDriver());
		for (WebElement list : navElementList) {
			if (list.getText().contentEquals(selectNavElement)) {
				actions.moveToElement(list).perform();
//				waitABit(2000);
//				System.out.println("Merge pana aici 1");
			}
		}
	}
	
	public void clickNavElement(String selectNavElement) {
		List<WebElement> navElementList = navElementContainer.findElements(
				By.xpath("//*[@id='nav']/ol/li/a"));
		Actions actions = new Actions(getDriver());
		for (WebElement list : navElementList) {
			if (list.getText().contentEquals(selectNavElement)) {
				actions.moveToElement(list).click();
				System.out.println("Merge prin random pana aici");
				waitABit(2000);
			}
		}
	}
	
	public void selectNavDropdownElement(String selectNavDropdownElement) {
		List<WebElement> navElementDropdownList = navElementDropdownContainer.findElements(
				By.xpath("//*[@id='nav']/ol/li[2]/ul/li/a"));
		Actions actions = new Actions(getDriver());
		for (WebElement list : navElementDropdownList) {
			if (list.getText().contentEquals(selectNavDropdownElement)) {
//				list.click();
				actions.moveToElement(list).click();
				waitABit(2000);
				System.out.println("Merge pana aici 2");
			}
		}
	}
	
	public void clickNavWomenElement(){
		navWomenElement.click();
	}
	
	public void clickNavMenElement(){
		navMenElement.click();
	}
	
	public void clickNavAccessoriesElement(){
		navAccessoriesElement.click();
	}
	
	public void clickNavHomedecorElement(){
		navHomedecorElement.click();
	}
	
	public void clickNavSaleElement(){
		navSaleElement.click();
	}
	
	public void clickNavVipElement(){
		navVipElement.click();
	}
}
