package com.pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class SubcategoryPage extends PageObject{
	
	@FindBy (css=".breadcrumbs > ul > li:nth-child(2) > a")
	public WebElement breadCrumbsToCategory;
	
	@FindBy (css="li:nth-child(1) > div > div.actions > a")
	public WebElement viewDetailsButton;

	@FindBy (css="li:nth-child(1) > div > div.actions > ul > li:nth-child(1) > a")
	public WebElement addToWishlistLink;
	
	@FindBy (css="li:nth-child(1) > div > div.actions > ul > li:nth-child(2) > a")
	public WebElement addToCompareLink;
	
	@FindBy (css="ul > li.item")
	public WebElement productContainer;
	
	
	public void clickOnBreadCrumbsToCategory(){
		breadCrumbsToCategory.click();
	}
	
	public void clickOnViewDetailsButton(){
		viewDetailsButton.click();
	}
	
	public void clickOnAddToWishlistLink(){
		addToWishlistLink.click();
	}
	
	public void clickOnAddToCompareLink(){
		addToCompareLink.click();
	}
	
	public int checkForProducts(){
		List<WebElement> productsList = productContainer.findElements(
				By.xpath("//*[@id='top']/body/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/ul/li"));
		return productsList.size();
	}
	
	
}
