package com.pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class CheckoutPage extends PageObject{
	
	@FindBy(css = "#checkout-step-login > div > div.col-1 > ul > li:nth-child(1) > label")
	public WebElement chechoutAsGuest;
	
	@FindBy(css = "#onepage-guest-register-button")
	public WebElement continueButton;
	
	@FindBy(xpath="//*[@id='billing:firstname']")
	public WebElement firstnameInput;
		
	@FindBy(xpath="//*[@id='billing:lastname']")
	public WebElement lastnameInput;
	
	@FindBy(xpath="//*[@id='billing:company']")
	public WebElement companyInput;
		
	@FindBy(xpath="//*[@id='billing:email']")
	public WebElement emailInput;
		
	@FindBy(xpath="//*[@id='billing:street1']")
	public WebElement addressInput;
		
	@FindBy(xpath="//*[@id='billing:city']")
	public WebElement cityInput;
		
	@FindBy(xpath="//*[@id='billing:country_id']")
	public WebElement countryDropdown;
	
	@FindBy(xpath="//*[@id='billing:country_id']/option[181]")
	public WebElement countryDropdownOption;
	
	@FindBy(xpath="//*[@id='billing:region_id']")
	public WebElement regionDropdown;
	
	@FindBy(xpath="//*[@id='billing:region_id']/option[15]")
	public WebElement regionDropdownOption;
		
	@FindBy(xpath="//*[@id='billing:postcode']")
	public WebElement zipcodeInput;
	
	@FindBy(xpath="//*[@id='billing:telephone']")
	public WebElement telephoneInput;
	
	@FindBy(css = "#co-billing-form > div > ul > li:nth-child(2) > label")
	public WebElement shipToAddressRadio;
	
	@FindBy(css = "#billing-buttons-container > button")
	public WebElement billingInfoContinueButton;
		
	@FindBy(css = "#checkout-shipping-method-load > dl > dd > ul > li:nth-child(1) > label")
	public WebElement firstShippingMethodRadio;
	
	@FindBy(css = "#shipping-method-buttons-container > button")
	public WebElement shippingContinueButton;
		
	@FindBy(css = "#payment-buttons-container > button")
	public WebElement paymentContinueButton;
		
	@FindBy(css = "#review-buttons-container > button")
	public WebElement placeOrderButton;
		
	@FindBy(css = "#review-buttons-container > button")
	public WebElement continueShoppingButton;
	
	
	
	public void clickOnChechoutAsGuest(){
		chechoutAsGuest.click();
	}
	
	public void clickOnContinueButton(){
		continueButton.click();
	}
	
	public void enterFirstname(String text){
		firstnameInput.sendKeys(text);
	}
	
	public void enterLastname(String text){
		lastnameInput.sendKeys(text);
	}
	
	public void enterCompany(String text){
		companyInput.sendKeys(text);
	}
		
	public void enterEmail(String text){
		emailInput.sendKeys(text);
	}
		
	public void enterAddress(String text){
		addressInput.sendKeys(text);
	}
		
	public void enterCity(String text){
		cityInput.sendKeys(text);
	}
	
	public void clickOnCountryDropdown(){
		countryDropdown.click();
	}
	
	public void clickOnCountryDropdownOption(){
		countryDropdownOption.click();
	}
	
	public void clickOnRegionDropdownn(){
		regionDropdown.click();
	}
	
	public void clickOnRegionDropdownnOption(){
		regionDropdownOption.click();
	}
	
	public void enterZipcode(String text){
		zipcodeInput.sendKeys(text);
	}
	
	public void enterTelephone(String text){
		telephoneInput.sendKeys(text);
	}

	public void clickOnShipToAddressRadio(){
		shipToAddressRadio.click();
	}
	
	public void clickOnBillingInfoContinueButton(){
		billingInfoContinueButton.click();
		waitABit(5000);
	}
		
	public void clickOnFirstShippingMethodRadio(){
		firstShippingMethodRadio.click();
	}
	
	public void clickOnShippingContinueButton(){
		shippingContinueButton.click();
	}
	
	public void clickOnPaymentContinueButton(){
		paymentContinueButton.click();
	}
	
	public void clickOnPlaceOrderButton(){
		placeOrderButton.click();
	}		
	
	public void clickOnContinueShoppingButton(){
		continueShoppingButton.click();
	}			
}
