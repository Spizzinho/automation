package com.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class MainCategoryPage extends PageObject {
	
	@FindBy (xpath="//*[@id='top']/body/div/div[2]/div[2]/div/div[2]/ul/li[1]/a/img")
	WebElementFacade subcategory_1;

	@FindBy (xpath="//*[@id='top']/body/div/div[2]/div[2]/div/div[2]/ul/li[2]/a/img")
	WebElementFacade subcategory_2;
	
	@FindBy (xpath="//*[@id='top']/body/div/div[2]/div[2]/div/div[2]/ul/li[3]/a/img")
	WebElementFacade subcategory_3;
	
	@FindBy (xpath="//*[@id='top']/body/div/div[2]/div[2]/div/div[2]/ul/li[4]/a/img")
	WebElementFacade subcategory_4;
	
	
	public void clickOnSubcategoryOne(){
//		System.out.println("Merge pana aici 2");
		subcategory_1.click();
	}
	
	public void clickOnSubcategoryTwo(){
		subcategory_2.click();
	}
	
	public void clickOnSubcategoryThree(){
		subcategory_3.click();
	}
	
	public void clickOnSubcategoryFour(){
		subcategory_4.click();
	}
}
